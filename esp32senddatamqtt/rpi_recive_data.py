import paho.mqtt.client as mqtt
import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BOARD)
buzzer=5
lampe =7
im1=11
im2=13
enable=15
GPIO.setwarnings(False)
GPIO.setup(lampe,GPIO.OUT)
GPIO.setup(im1,GPIO.OUT)
GPIO.setup(im2,GPIO.OUT)
GPIO.setup(enable,GPIO.OUT)
GPIO.setup(buzzer,GPIO.OUT)
pwm=GPIO.PWM(enable,50)
pwm.start(100)
msg=""
def arret ():
    GPIO.output(enable,0)
    pwm.stop()
    print("arrét")
def ofenetre():
    global msg
    if msg!="fenetre ouvert":
        print("ok")
        GPIO.output(im2,0)
        GPIO.output(im1,1)
        GPIO.output(enable,1)
        time.sleep(10)
        print("o fent")
        #arret()
        msg="fenetre ouvert"
        print(msg)
    else:
        print("arret")
        arret()
def ffenetre():
    global msg
    if msg=="fenetre ouvert":
        print("ok")
        GPIO.output(im2,1)
        GPIO.output(im1,0)
        GPIO.output(enable,1)
        time.sleep(10)
        print("f fent")
        #arret()
        msg="fenetre fermé"
        print(msg)
    else:
        print("arret")
        arret()
def eteintlampe():
    GPIO.output(lampe,0)
    print("non allume")
def allumeLampe():
    GPIO.output(lampe,1)
    GPIO.output(buzzer,0)
    print("allumee")
def alarme():
    GPIO.output(buzzer,1)
    GPIO.output(lampe,0)
    print("alarm on ")
def alarmedisable():
    GPIO.output(buzzer,0)
    print("alarm off")
def on_connect(client, userdata, flags, rc):
    print(f"Connected with result code {rc}")
    client.subscribe("fume")
# The callback function, it will be triggered when receiving messages
def on_message(client, userdata, msg):
    print(f"{msg.topic} {msg.payload}")
    print(int(msg.payload))
    if int(msg.payload)>=2230:
        alarme()
        eteintlampe()
        ofenetre()
    else:
        alarmedisable()
        ffenetre()
        allumeLampe()
client = mqtt.Client()
client.on_connect = on_connect
while True:
    client.on_message = on_message
    client.will_set('raspberry/status', b'{"status": "Off"}')
    client.connect("192.168.1.37", 1883, 60)
    client.loop_forever()
